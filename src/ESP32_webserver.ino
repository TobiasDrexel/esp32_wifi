#include <WiFi.h>
#define LED_PIN 2

const char* ssid     = "EMBEDDEDLAB";
const char* password = "mos23EMBLab";

WiFiServer serverWiFi (80);

void wifiSend (WiFiClient client) {   
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html"); 
  client.println();  
  
  client.print("<!DOCTYPE HTML>");
	client.print("<head><title>ESP32 Webserver</title></head><html>");
  client.print("Klick <a href=\"/H\">an</a> oder <a href=\"/L\">aus</a> !");
  client.print("</html>");
  client.println();
}

void wifiReceive (WiFiClient client) {
  String currentLine = "";
  while (client.connected()) {
    if (client.available()) {
      char c = client.read();
      if (c == '\n') {
        if (currentLine.length() == 0) {
          wifiSend(client);            
          break;
        }
        else {
         currentLine = "";
        }
      } else if (c != '\r') {
        currentLine += c;
      }
      if (currentLine.endsWith("GET /H")) {
        digitalWrite(LED_PIN, HIGH);
      }
      if (currentLine.endsWith("GET /L")) {
        digitalWrite(LED_PIN, LOW);
      }  
    }
  }
  
  client.stop();
}

void setup() {
  pinMode(LED_PIN, OUTPUT);
  
  IPAddress local_ip(192, 168, 178, 42);
  IPAddress gateway(192, 168, 178, 1);
  IPAddress subnet_mask(255, 255, 0, 0);

  if( !WiFi.config(local_ip, gateway, subnet_mask) )
	{
		return;
	}

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    }

  serverWiFi.begin();
}

void loop() {
  WiFiClient client = serverWiFi.available();
  if (client) {
    wifiReceive (client);
  }

  delay(1);
}