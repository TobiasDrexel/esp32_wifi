# ESP32_WiFi

## About the repository
This repository contains example Arduino sketches for the ESP32 to demonstrate the usage of its WiFi module.
Additionally, documentation is provided. Please note that the documentation is written in German.

The contents in this repository have been created for the lecture *Applications of modern ICT* of the [DHBW
CAS](https://www.cas.dhbw.de/) Electrical Engineering Master program.

## License
[MIT](https://choosealicense.com/licenses/mit/) license is applied to the contents of this repository.