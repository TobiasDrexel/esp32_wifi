const char* ssid     = "EMBEDDEDLAB";
const char* password = "mos23EMBLab";

IPAddress local_ip(192, 168, 178, 42); // Static IP
IPAddress gateway(192, 168, 178, 1);   // Router IP
IPAddress subnet_mask(255, 255, 0, 0); // Subnet mask

void setup() {
	if( !WiFi.config(local_ip, gateway, subnet_mask) ) {
		// Error occurred
		return;
	}
	// Wifi.begin...
}